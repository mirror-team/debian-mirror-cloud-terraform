module "archive_debug" {
  source = "./archive"
  enable = "${var.enable_archive_debug}"
  env = "${var.env}"
  archive = "debian-debug"
  prefix_backend = "mirror-${var.env}-b-d"
  prefix_syncproxy = "mirror-${var.env}-s-d"

  disk_size = "${var.disk_size_archive_debug}"
  disk_snapshot = "${var.disk_snapshot_archive_debug}"
  instance_count_backend = "${var.instance_count_backend}"
  instance_image = "${var.instance_image}"
  instance_type_backend = "${var.instance_type_backend}"
  instance_type_syncproxy = "${var.instance_type_syncproxy}"
  lb_health_checks = ["${google_compute_http_health_check.default.self_link}"]
  zone_syncproxy = "${var.zone_syncproxy}"
}

module "archive_main" {
  source = "./archive"
  enable = 1
  env = "${var.env}"
  archive = "debian"
  prefix_backend = "mirror-${var.env}-b-m"
  prefix_syncproxy = "mirror-${var.env}-s-m"

  disk_size = "${var.disk_size_archive_main}"
  disk_snapshot = "${var.disk_snapshot_archive_main}"
  instance_count_backend = "${var.instance_count_backend}"
  instance_image = "${var.instance_image}"
  instance_type_backend = "${var.instance_type_backend}"
  instance_type_syncproxy = "${var.instance_type_syncproxy}"
  lb_health_checks = ["${google_compute_http_health_check.default.self_link}"]
  zone_syncproxy = "${var.zone_syncproxy}"
}

module "archive_ports" {
  source = "./archive"
  enable = "${var.enable_archive_ports}"
  env = "${var.env}"
  archive = "debian-ports"
  prefix_backend = "mirror-${var.env}-b-p"
  prefix_syncproxy = "mirror-${var.env}-s-p"

  disk_size = "${var.disk_size_archive_ports}"
  disk_snapshot = "${var.disk_snapshot_archive_ports}"
  instance_count_backend = "${var.instance_count_backend}"
  instance_image = "${var.instance_image}"
  instance_type_backend = "${var.instance_type_backend}"
  instance_type_syncproxy = "${var.instance_type_syncproxy}"
  lb_health_checks = ["${google_compute_http_health_check.default.self_link}"]
  zone_syncproxy = "${var.zone_syncproxy}"
}

module "archive_security" {
  source = "./archive"
  enable = "${var.enable_archive_security}"
  env = "${var.env}"
  archive = "debian-security"
  prefix_backend = "mirror-${var.env}-b-s"
  prefix_syncproxy = "mirror-${var.env}-s-s"

  disk_size = "${var.disk_size_archive_security}"
  disk_snapshot = "${var.disk_snapshot_archive_security}"
  instance_count_backend = "${var.instance_count_backend}"
  instance_image = "${var.instance_image}"
  instance_type_backend = "${var.instance_type_backend}"
  instance_type_syncproxy = "${var.instance_type_syncproxy}"
  lb_health_checks = ["${google_compute_http_health_check.default.self_link}"]
  zone_syncproxy = "${var.zone_syncproxy}"
}
