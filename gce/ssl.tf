resource "random_id" "certificate-lb-global" {
  byte_length = 4
  prefix = "mirror-${var.env}-b-snakeoil-"
}

resource "google_compute_ssl_certificate" "lb-global" {
  name = "${random_id.certificate-lb-global.dec}"
  certificate = "${var.lb_global_ssl_certificate}"
  private_key = "${var.lb_global_ssl_private_key}"

  lifecycle {
    create_before_destroy = true
  }
}
