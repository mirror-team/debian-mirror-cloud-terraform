resource "google_compute_http_health_check" "default" {
  name = "mirror-${var.env}-b"
  request_path = "/.online"
  check_interval_sec = 30
  timeout_sec = 15
}

resource "google_compute_url_map" "default" {
  name = "mirror-${var.env}-b"
  default_service = "${module.archive_main.backend_service[0]}"

  host_rule {
    hosts        = ["*"]
    path_matcher = "all"
  }

  path_matcher {
    name            = "all"
    default_service = "${module.archive_main.backend_service[0]}"

    path_rule {
      paths   = ["/debian", "/debian/*"]
      service = "${module.archive_main.backend_service[0]}"
    }

    path_rule {
      paths   = ["/debian-debug", "/debian-debug/*"]
      service = "${element(concat(module.archive_debug.backend_service, module.archive_main.backend_service), 0)}"
    }

    path_rule {
      paths   = ["/debian-ports", "/debian-ports/*"]
      service = "${element(concat(module.archive_ports.backend_service, module.archive_main.backend_service), 0)}"
    }

    path_rule {
      paths   = ["/debian-security", "/debian-security/*"]
      service = "${element(concat(module.archive_security.backend_service, module.archive_main.backend_service), 0)}"
    }
  }

  host_rule {
    hosts        = ["security.debian.org"]
    path_matcher = "security"
  }

  path_matcher {
    name            = "security"
    default_service = "${element(concat(module.archive_security.backend_service, module.archive_main.backend_service), 0)}"
  }
}

resource "google_compute_target_http_proxy" "default" {
  name = "mirror-${var.env}-b"
  url_map = "${google_compute_url_map.default.self_link}"
}

resource "google_compute_target_https_proxy" "default" {
  name = "mirror-${var.env}-b"
  url_map = "${google_compute_url_map.default.self_link}"
  ssl_certificates = ["${google_compute_ssl_certificate.lb-global.self_link}"]
}

resource "google_compute_global_forwarding_rule" "http" {
  count = "${length(var.lb_global_ip)}"
  name = "mirror-${var.env}-b-http-${count.index}"
  target = "${google_compute_target_http_proxy.default.self_link}"
  ip_address = "${var.lb_global_ip[count.index]}"
  port_range = "80"
}

resource "google_compute_global_forwarding_rule" "https" {
  count = "${length(var.lb_global_ip)}"
  name = "mirror-${var.env}-b-https-${count.index}"
  target = "${google_compute_target_https_proxy.default.self_link}"
  ip_address = "${var.lb_global_ip[count.index]}"
  port_range = "443"
}

resource "google_pubsub_topic" "lb-global-log" {
  name = "mirror-${var.env}-b-log"
}

data "google_iam_policy" "pubsub-lb-global-log" {
  binding {
    role = "roles/pubsub.publisher"
    members = [
      "${google_logging_project_sink.lb-global-log.writer_identity}",
    ]
  }
}

resource "google_pubsub_topic_iam_policy" "lb-global-log" {
  topic = "mirror-${var.env}-b-log"
  policy_data = "${data.google_iam_policy.pubsub-lb-global-log.policy_data}"
}

resource "google_logging_project_sink" "lb-global-log" {
  name = "mirror-${var.env}-b-log"
  destination = "pubsub.googleapis.com/projects/${var.provider_project}/topics/${google_pubsub_topic.lb-global-log.name}"
  filter = "resource.type=http_load_balancer resource.labels.url_map_name=mirror-${var.env}-b severity>=INFO"
  unique_writer_identity = true
}
