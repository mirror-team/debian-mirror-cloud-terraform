provider "google" {
  project = "${var.provider_project}"
  region = "us-central1"
}
