resource "google_compute_firewall" "internal" {
  name = "mirror-${var.env}-allow-internal"
  network = "mirror"

  allow {
    protocol = "all"
  }

  source_tags = [
    "mirror-${var.env}",
  ]
  target_tags = [
    "mirror-${var.env}",
  ]
}

resource "google_compute_firewall" "external-monitor" {
  name = "mirror-${var.env}-monitor-allow-external"
  network = "mirror"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = [
    "0.0.0.0/0",
  ]
  target_tags = [
    "mirror-${var.env}-monitor",
  ]
}

resource "google_compute_firewall" "lb-backend" {
  name = "mirror-${var.env}-backend-allow-lb"
  network = "mirror"

  allow {
    protocol = "tcp"
    ports    = [
      "80",
      "8080",
    ]
  }

  source_ranges = [
    "130.211.0.0/22",
    "35.191.0.0/16",
  ]
  target_tags = [
    "mirror-${var.env}-backend",
  ]
}
