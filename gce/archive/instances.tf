resource "google_compute_instance" "syncproxy" {
  count = "${var.enable}"
  name = "${var.prefix_syncproxy}"
  zone = "${var.zone_syncproxy}"
  machine_type = "${var.instance_type_syncproxy}"
  allow_stopping_for_update = true

  labels = {
    archive = "${var.archive}"
    environment = "${var.env}",
    purpose = "syncproxy",
    project = "mirror",
  }

  tags = [
    "mirror-${var.env}",
    "mirror-${var.env}-syncproxy",
  ]

  boot_disk {
    initialize_params {
      image = "${var.instance_image}"
      type = "pd-ssd"
    }
  }

  attached_disk {
    source = "${google_compute_disk.syncproxy.*.self_link[count.index]}"
  }

  network_interface {
    subnetwork = "mirror"
    access_config {
      // Ephemeral IP
    }
  }
}

resource "google_compute_disk" "syncproxy" {
  count = "${var.enable}"
  name = "${var.prefix_syncproxy}-data"
  zone = "${var.zone_syncproxy}"
  size = "${var.disk_size}"
  snapshot = "${var.disk_snapshot}"
  type = "pd-ssd"

  timeouts {
    create = "1h"
  }

  lifecycle {
    ignore_changes = [ "snapshot" ]
  }
}
