output "instances" {
  value = ["${google_compute_instance.default.*.name}"]
}

output "group" {
  value = "${google_compute_instance_group.default.*.self_link}"
}
