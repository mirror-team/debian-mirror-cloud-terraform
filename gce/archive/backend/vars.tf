variable "enable" { }
variable "env" { }
variable "archive" { }
variable "prefix" { }

variable "disk_size" { }
variable "disk_snapshot" { }
variable "instance_count" { }
variable "instance_image" { }
variable "instance_shift" { }
variable "instance_type" { }
variable "zone" { }
