variable "provider_project" { }
variable "env" { }

variable "enable_archive_debug" {
  default = 0
}
variable "enable_archive_ports" {
  default = 0
}
variable "enable_archive_security" {
  default = 0
}

variable "disk_size_archive_debug" {
  default = 10
}
variable "disk_size_archive_main" {
  default = 1750
}
variable "disk_size_archive_ports" {
  default = 10
}
variable "disk_size_archive_security" {
  default = 150
}
variable "disk_size_monitor" {
  default = 10
}
variable "disk_snapshot_archive_debug" {
  default = ""
}
variable "disk_snapshot_archive_main" {
  default = ""
}
variable "disk_snapshot_archive_ports" {
  default = ""
}
variable "disk_snapshot_archive_security" {
  default = ""
}
variable "instance_count_backend" {
  type = "map"
}
variable "instance_image" { }
variable "instance_ip_jump" {
  default = ""
}
variable "instance_ip_monitor" {
  default = ""
}
variable "instance_type_backend" { }
variable "instance_type_monitor" { }
variable "instance_type_syncproxy" { }
variable "lb_global_ip" {
  type = "list"
  default = [""]
}
variable "lb_global_ssl_certificate" { }
variable "lb_global_ssl_private_key" { }
variable "zone_monitor" { }
variable "zone_jump" { }
variable "zone_syncproxy" { }
