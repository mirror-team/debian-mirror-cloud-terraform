module "gce" {
  source = "../../../gce"

  provider_project = "debian-infra"
  env = "prod"

  enable_archive_security = 1
  disk_snapshot_archive_main = "mirror-main-20180423"
  disk_snapshot_archive_security = "mirror-security-20180112"
  instance_count_backend = {
    australia-southeast1 = 2
    europe-west4 = 2
    us-central1 = 2
  }
  instance_image = "debian-cloud/debian-9"
  instance_ip_jump = "35.204.36.43"
  instance_ip_monitor = "35.204.17.73"
  instance_type_backend = "n1-standard-2"
  instance_type_monitor = "n1-standard-1"
  instance_type_syncproxy = "n1-standard-2"
  lb_global_ip = [
    "35.190.65.49",
    "2600:1901:0:516::",
  ]
  lb_global_ssl_certificate = "${file("snakeoil_b.crt")}"
  lb_global_ssl_private_key = "${file("snakeoil_b.key")}"
  zone_jump = "europe-west4-a"
  zone_monitor = "europe-west4-a"
  zone_syncproxy = "europe-west4-a"
}

terraform {
  backend "gcs" {
    bucket = "debian-infra-mgt"
    prefix = "terraform-state/mirror/prod/gce"
  }
}
