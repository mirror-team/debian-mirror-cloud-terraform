resource "aws_route53_zone" "default" {
  provider = "aws.global"
  name = "${var.domain}."
  comment = ""

  tags {
    Environment = "${var.env}",
    Project = "mirror",
  }
}
