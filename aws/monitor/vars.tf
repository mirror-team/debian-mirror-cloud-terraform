variable "provider_access_key" { }
variable "provider_secret_key" { }
variable "env" { }
variable "region" { }
variable "zone" { }

variable "instance_ami" { }
variable "instance_type" { }
variable "route53_zone" { }
variable "subnet" { }
variable "vpc" { }
