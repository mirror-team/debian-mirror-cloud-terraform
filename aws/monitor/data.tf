data "aws_ami" "debian" {
  provider = "aws.monitor"
  most_recent = true
  filter {
    name = "name"
    values = ["${var.instance_ami}"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["379101102735"]
}
