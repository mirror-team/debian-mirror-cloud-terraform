variable "provider_access_key" { }
variable "provider_secret_key" { }
variable "env" { }
variable "region" { }
variable "zone" { }

variable "disk_snapshot_main" { }
variable "disk_snapshot_security" { }
variable "disk_size_main" { }
variable "disk_size_security" { }
variable "instance_ami" { }
variable "instance_type" { }
variable "route53_zone" { }
variable "subnet" { }
variable "vpc" { }
