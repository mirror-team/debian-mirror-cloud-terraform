resource "aws_instance" "syncproxy-main" {
  provider = "aws.syncproxy"
  availability_zone = "${var.zone}"
  ami = "${data.aws_ami.debian.id}"
  instance_type = "${var.instance_type}"
  ipv6_address_count = 1
  subnet_id = "${var.subnet}"
  vpc_security_group_ids = ["${aws_security_group.syncproxy.id}"]
  key_name = "waldi" # FIXME

  lifecycle {
    ignore_changes = ["ami"]
  }

  tags {
    Environment = "${var.env}",
    Name = "mirror-${var.env}-s-m",
    Project = "mirror"
    Purpose = "syncproxy-main",
  }
}

resource "aws_ebs_volume" "syncproxy-main" {
  provider = "aws.syncproxy"
  availability_zone = "${var.zone}"
  size = "${var.disk_size_main}"
  snapshot_id = "${join("", data.aws_ebs_snapshot.main.*.snapshot_id)}"
  type = "gp2"

  lifecycle {
    ignore_changes = [ "snapshot_id" ]
  }

  tags {
    Environment = "${var.env}",
    Name = "mirror-${var.env}-s-m-data"
    Project = "mirror"
  }
}

resource "aws_volume_attachment" "syncproxy-main" {
  provider = "aws.syncproxy"
  device_name = "/dev/sdb"
  volume_id = "${aws_ebs_volume.syncproxy-main.id}"
  instance_id = "${aws_instance.syncproxy-main.id}"
}

resource "aws_route53_record" "syncproxy-main" {
  provider = "aws.global"
  zone_id = "${var.route53_zone}"
  name = "s-m"
  type = "AAAA"
  ttl = 3600
  records = ["${aws_instance.syncproxy-main.*.ipv6_addresses[count.index]}"]
}
