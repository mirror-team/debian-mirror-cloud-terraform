data "aws_ami" "debian" {
  provider = "aws.backend"
  most_recent = true
  filter {
    name = "name"
    values = ["${var.instance_ami}"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["379101102735"]
}

data "aws_ebs_snapshot" "main" {
  provider = "aws.backend"
  count = "${var.disk_snapshot_main != "" && var.instance_count > 0 ? 1 : 0}"
  most_recent = true
  owners = ["self"]
  filter {
    name = "tag:Name"
    values = ["${var.disk_snapshot_main}"]
  }
}

data "aws_ebs_snapshot" "security" {
  provider = "aws.backend"
  count = "${var.disk_snapshot_main != "" && var.instance_count > 0 ? 1 : 0}"
  most_recent = true
  owners = ["self"]
  filter {
    name = "tag:Name"
    values = ["${var.disk_snapshot_security}"]
  }
}
