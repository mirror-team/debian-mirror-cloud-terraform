variable "provider_access_key" { }
variable "provider_secret_key" { }
variable "env" { }
variable "zones" {
  type = "map"
}

variable "domain" { }
variable "disk_size_main" { }
variable "disk_size_security" { }
variable "disk_snapshot_main" { }
variable "disk_snapshot_security" { }
variable "instance_ami" { }
variable "instance_count_backend" {
  type = "map"
}
variable "instance_type_backend" { }
variable "instance_type_monitor" { }
variable "instance_type_syncproxy" { }
