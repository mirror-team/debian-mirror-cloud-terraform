module "network-ap-southeast-2" {
  source = "./network"
  provider_access_key = "${var.provider_access_key}"
  provider_secret_key = "${var.provider_secret_key}"
  env = "${var.env}"
  region = "ap-southeast-2"
  region_suffix = "apse2"
  zones = "${var.zones["ap-southeast-2"]}"

  lb_count = "${lookup(var.instance_count_backend, "ap-southeast-2", 0) > 0 ? 1 : 0}"
  route53_zone = "${aws_route53_zone.default.zone_id}"
  cidr_blocks = [
    "${module.network-ap-southeast-2.cidr_blocks}",
    "${module.network-eu-central-1.cidr_blocks}",
    "${module.network-sa-east-1.cidr_blocks}",
    "${module.network-us-west-2.cidr_blocks}",
  ]
}

module "network-eu-central-1" {
  source = "./network"
  provider_access_key = "${var.provider_access_key}"
  provider_secret_key = "${var.provider_secret_key}"
  env = "${var.env}"
  region = "eu-central-1"
  region_suffix = "euc1"
  zones = "${var.zones["eu-central-1"]}"

  lb_count = "${lookup(var.instance_count_backend, "eu-central-1", 0) > 0 ? 1 : 0}"
  route53_zone = "${aws_route53_zone.default.zone_id}"
  cidr_blocks = [
    "${module.network-ap-southeast-2.cidr_blocks}",
    "${module.network-eu-central-1.cidr_blocks}",
    "${module.network-sa-east-1.cidr_blocks}",
    "${module.network-us-west-2.cidr_blocks}",
  ]
}

module "network-sa-east-1" {
  source = "./network"
  provider_access_key = "${var.provider_access_key}"
  provider_secret_key = "${var.provider_secret_key}"
  env = "${var.env}"
  region = "sa-east-1"
  region_suffix = "sae1"
  zones = "${var.zones["sa-east-1"]}"

  lb_count = "${lookup(var.instance_count_backend, "sa-east-1", 0) > 0 ? 1 : 0}"
  route53_zone = "${aws_route53_zone.default.zone_id}"
  cidr_blocks = [
    "${module.network-ap-southeast-2.cidr_blocks}",
    "${module.network-eu-central-1.cidr_blocks}",
    "${module.network-sa-east-1.cidr_blocks}",
    "${module.network-us-west-2.cidr_blocks}",
  ]
}

module "network-us-west-2" {
  source = "./network"
  provider_access_key = "${var.provider_access_key}"
  provider_secret_key = "${var.provider_secret_key}"
  env = "${var.env}"
  region = "us-west-2"
  region_suffix = "usw2"
  zones = "${var.zones["us-west-2"]}"

  lb_count = "${lookup(var.instance_count_backend, "us-west-2", 0) > 0 ? 1 : 0}"
  route53_zone = "${aws_route53_zone.default.zone_id}"
  cidr_blocks = [
    "${module.network-ap-southeast-2.cidr_blocks}",
    "${module.network-eu-central-1.cidr_blocks}",
    "${module.network-sa-east-1.cidr_blocks}",
    "${module.network-us-west-2.cidr_blocks}",
  ]
}
